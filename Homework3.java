package homework;

public class Homework3 {
	public static void main(String[] args) {
		int n = 5;

		// drow9(n);
		// drow10(n);
		// drow11(n);
		// drow12(n);
		// drow13(n);
		// drow14(n);
		// drow15(n);
		// drow16(n);
		drow17(n);

	}

	public static int drow9(int count) {

		for (int i = 0; i < count; i++) {
			int m = i * 2;
			System.out.println(m);
		}
		return count;
	}

	public static int drow10(int count) {

		for (int i = 1; i <= count; i++) {
			int m = i * 2;
			System.out.println(m);
		}
		return count;
	}

	public static int drow11(int count) {
		int k = 1;
		for (int i = 1; i <= count; i++) {
			for (int m = 1; m <= count; m++, k++) {
				System.out.print(k);
			}
			System.out.print("\n");
		}
		System.out.println("");

		return count;
	}

	public static int drow12(int count) {
		String[] result = new String[count];
		int i;
		for (i = 0; i < count; i++)
			result[i] = "*";
		for (i = 0; i < count; i++) {
			result[i] = "-";
			for (int m = 0; m < count; m++)
				System.out.print(result[m]);
			result[i] = "*";

			System.out.println();
		}

		return count;
	}

	public static int drow13(int count) {
		String[] result = new String[count];
		int i;
		for (i = 0; i < count; i++)
			result[i] = "-";
		for (i = 0; i < count; i++) {
			result[i] = "*";
			for (int m = 0; m < count; m++)
				System.out.print(result[m]);
			result[i] = "-";

			System.out.println();
		}
		return count;
	}

	public static int drow14(int count) {
		for (int i = 0; i < count; i++) {
			for (int m = 0; m <= i; m++) {
				System.out.print(" *");
			}
			for (int m = i; m < count - 1; m++) {
				System.out.print("-");
			}
			System.out.println(" ");
		}
		return count;
	}

	public static int drow15(int count) {
		for (int i = 1; i <= count; i++) {
			for (int m = i; m <= count; m++) {

				System.out.print("*");
			}
			for (int m = 1; m <= i - 1; m++) {
				System.out.print("-");
			}
			System.out.println(" ");
		}
		return count;
	}

	public static int drow16(int count) {
		for (int i = 0; i < count; i++) {
			for (int m = 0; m <= i; m++) {
				System.out.print("*");
			}
			for (int m = i; m <= count - 2; m++) {
				System.out.print("-");
			}
			System.out.println("");
		}
		for (int i = 1; i <= count - 1; i++) {
			for (int m = 1; m <= (count - i); m++) {
				System.out.print("*");
			}
			for (int m = 1; m <= i; m++) {
				System.out.print("-");
			}
			System.out.println("");
		}

		return count;
	}

	public static int drow17(int count) {
		for (int i = 0; i < count; i++) {
			for (int m = 0; m <= i; m++) {
				System.out.print("" + (i + 1));
			}
			for (int m = i; m <= count - 2; m++) {
				System.out.print("-");
			}
			System.out.println("");
		}
		for (int i = 1; i <= count - 1; i++) {
			for (int m = 1; m <= (count - i); m++) {
				System.out.print("" + (count - i));
			}
			for (int m = 1; m <= i; m++) {
				System.out.print("-");
			}
			System.out.println("");
		}
		return count;
	}

}
