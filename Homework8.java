package homework;

import java.util.Scanner;

public class Homework8 {
	public static void main(String[] args) {

		int x = 5;

		drow1(x);
		// drow2(x);
		// drow3(x);
		// drow4(x);
		// drow5(x);
		// drow6(x);
		// drow7(x);
		// drow8(x);
	}

	public static int drow1(int number) {
		for (int x = 0; x < number; x++) {
			System.out.print("*");
		}
		return number;
	}

	public static int drow2(int number) {
		for (int x = 0; x < number; x++) {
			for (int x1 = 0; x1 < number; x1++) {
				System.out.print("*");
			}
			System.out.println("");
		}
		return number;
	}

	public static int drow3(int number) {
		for (int x = 1; x <= number; x++) {
			for (int x1 = 1; x1 <= number; x1++) {
				System.out.print(x1);
			}
			System.out.println("");
		}
		return number;
	}

	public static int drow4(int number) {
		for (int num1 = number; num1 > 0; num1--) {
			for (int num2 = number; num2 > 0; num2--) {
				System.out.print(num2);
			}
			System.out.println("");
		}
		return number;
	}

	public static int drow5(int number) {
		for (int num1 = 1; num1 <= number; num1++) {
			for (int num2 = 1; num2 <= number; num2++) {
				System.out.print(num1);
			}
			System.out.println("");
		}
		return number;
	}

	public static int drow6(int number) {
		for (int num1 = number; num1 >= 1; num1--) {
			for (int num2 = 1; num2 <= number; num2++) {
				System.out.print(num1);
			}
			System.out.println("");
		}
		return number;
	}

	public static int drow7(int number) {
		int k = 1;
		for (int num1 = 1; num1 <= number; num1++) {
			for (int num2 = 1; num2 <= number; num2++, k++) {
				System.out.printf("%3d", k);
			}
			System.out.print("\n");
		}

		System.out.println("");
		return number;
	}

	public static int drow8(double number) {
		double k = Math.pow(number, 2);
		for (double num1 = number; num1 >= 1.0; num1--) {
			for (double num2 = number; num2 >= 1.0; num2--, k--) {
				System.out.printf("%3d", (int) k);
			}
			System.out.print("\n");
		}
		System.out.println("");
		return (int) number;
	}

}
