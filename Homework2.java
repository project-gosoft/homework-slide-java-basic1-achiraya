package homework;

public class Homework2 {
	public static void main(String[] args) {

		multiplyTable();
	}

	public static void multiplyTable() {
		String[][] table = { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };
		int[] myArray = new int[table.length];

		for (int row = 0; row < table.length; row++) {
			for (int i = 0; i < table.length; i++) {
				myArray[row] = Integer.parseInt(table[row][i]);

				/* ผลลัพธ์จะไม่มีช่องว่าง ถ้าไม่มี %3d */
				System.out.printf("%3d", myArray[row] * 2);
			}
			/* ผลลัพธ์จะเรียงกันบนบรรทัดเดียว */
			System.out.println("");
		}
	}
}
